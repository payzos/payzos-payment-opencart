[![Payzos logo](/.gitassets/img/logo.png "Payzos logo")](https://payzos.io)

# Payzos payment for Opencart

Payzos is Tezos payment service

## Installation

### OpenCart Cloud :

-   Search for the `Payzos` extension in the extension marketplace.
-   Proceed to Extensions >> Extensions and select Payments. Then, install `Payzos`.
-   Configure payment extension accordingly. Please view below for more information on configuring the extension.

### OpenCart 3 :

-   Go to Admin >> Extensions >> Installer to upload the extension zip file.
-   Proceed to Extensions >> Extensions and select Payments. Then, install `Payzos`.
-   Configure payment extension accordingly. Please view below for more information on configuring the extension.

## Configurations

![Payzos logo](/.gitassets/img/PayzosOpercartConfiguration.png "Payzos Opencart Configuration")

-   set your tezos public key
-   choice which status you want to set for order after success payment

## License

GPL V3
