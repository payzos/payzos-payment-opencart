<?php
// Heading
$_['heading_title'] = '<b style="color:#1E91CF;">Payzos</b>';

// Text
$_['text_extension'] = 'Extensions';
$_['text_success'] = 'Success : Modificaions was successfully done!';
$_['text_payzos'] =
    '<a href="https://payzos.io" target="_blank"><img src="view/image/payment/payzos.png" alt="Payzos" title="Payzos" style="border: 1px solid #EEEEEE;" ></a>';
$_['text_edit'] = 'Edit Payzos Payment';

// Entry
$_['entry_wallet_hash'] = 'Tezos wallet hash';
$_['entry_status'] = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify payment payzos!';
$_['error_entry_wallet_hash'] = 'Walet hash is require';
$_['entry_order_status'] = 'order status';
$_['unvalid_wallet_hash'] = 'unvalid wallet hash';
