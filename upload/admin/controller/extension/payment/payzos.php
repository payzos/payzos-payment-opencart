<?php
class ControllerExtensionPaymentPayzos extends Controller
{
    private $error = [];

    public function install()
    {
    }

    public function index()
    {
        $this->install();
        $this->load->language('extension/payment/payzos');

        $this->document->setTitle(strip_tags($this->language->get('heading_title')));

        $this->load->model('setting/setting');

        if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {
            $this->model_setting_setting->editSetting('payment_payzos', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['pin'])) {
            $data['error_pin'] = $this->error['pin'];
        } else {
            $data['error_pin'] = '';
        }

        $data['breadcrumbs'] = [];

        $data['breadcrumbs'][] = [
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link(
                'common/dashboard',
                'user_token=' . $this->session->data['user_token'],
                true
            ),
        ];

        $data['breadcrumbs'][] = [
            'text' => $this->language->get('text_extension'),
            'href' => $this->url->link(
                'marketplace/extension',
                'user_token=' . $this->session->data['user_token'] . '&type=payment',
                true
            ),
        ];

        $data['breadcrumbs'][] = [
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link(
                'extension/payment/payzos',
                'user_token=' . $this->session->data['user_token'],
                true
            ),
        ];

        $data['action'] = $this->url->link(
            'extension/payment/payzos',
            'user_token=' . $this->session->data['user_token'],
            true
        );

        $data['cancel'] = $this->url->link(
            'marketplace/extension',
            'user_token=' . $this->session->data['user_token'] . '&type=payment',
            true
        );

        if (isset($this->request->post['payment_payzos_wallet_hash']) && $data['error_pin'] == '') {
            $data['payment_payzos_wallet_hash'] =
                $this->request->post['payment_payzos_wallet_hash'];
        } else {
            $data['payment_payzos_wallet_hash'] = $this->config->get('payment_payzos_wallet_hash');
        }

        if (isset($this->request->post['payment_payzos_order_status_id'])) {
            $data['payment_payzos_order_status_id'] =
                $this->request->post['payment_payzos_order_status_id'];
        } else {
            $data['payment_payzos_order_status_id'] = $this->config->get(
                'payment_payzos_order_status_id'
            );
        }
        $this->load->model('localisation/order_status');
        $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

        if (isset($this->request->post['payment_payzos_order_status_id'])) {
            $data['payment_payzos_order_status_id'] =
                $this->request->post['payment_payzos_order_status_id'];
        } else {
            $data['payment_payzos_order_status_id'] = $this->config->get(
                'payment_payzos_order_status_id'
            );
        }

        if (isset($this->request->post['payment_payzos_status'])) {
            $data['payment_payzos_status'] = $this->request->post['payment_payzos_status'];
        } else {
            $data['payment_payzos_status'] = $this->config->get('payment_payzos_status');
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/payment/payzos', $data));
    }

    protected function validate()
    {
        if (!$this->user->hasPermission('modify', 'extension/payment/payzos')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->request->post['payment_payzos_wallet_hash']) {
            $this->error['pin'] = $this->language->get('error_entry_wallet_hash');
        }
        $wallet_hash = $this->request->post['payment_payzos_wallet_hash'];

        require_once DIR_SYSTEM . 'library/payzos/PayzosApi.php';
        $payzos_api = new PayzosApi();

        if (!$payzos_api->validate_wallet_hash($wallet_hash)) {
            $this->error['pin'] = $this->language->get('unvalid_wallet_hash');
        }
        return !$this->error;
    }
}
