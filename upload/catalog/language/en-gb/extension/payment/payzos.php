<?php
// Text
$_['text_title'] = 'payzos Online Payment';
$_['text_wait'] = 'Please Wait!';
$_['text_connect'] = 'Connecting To payzos!';
$_['text_payment_id'] = 'Payment ID : ';
$_['text_payment_TX'] = 'Transaction Hash : ';
$_['text_payment_amount'] = 'amount : ';
$_['text_payment_date'] = 'date : ';
$_['button_view_cart'] = 'View Shopping Cart';
$_['button_complete'] = 'Complete Payment';
$_['error_order_id'] = 'Order Id Not Found !';
$_['error_payment_id'] = 'Payment Id Not Found !';
$_['error_payment_unvalid'] = 'Payment is unvalid';
$_['error_total'] = 'Payment total is unvalid';
$_['error_not_found'] = 'Request not found';
$_['error_verify'] = 'unvalid';
