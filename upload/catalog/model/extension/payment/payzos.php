<?php
class ModelExtensionPaymentPayzos extends Model
{
    public function getMethod($address)
    {
        $this->load->language('extension/payment/payzos');

        if ($this->config->get('payment_payzos_status')) {
            $status = true;
        } else {
            $status = false;
        }

        $method_data = [];

        if ($status) {
            $method_data = [
                'code' => 'payzos',
                'title' => $this->language->get('text_title'),
                'terms' => '',
                'sort_order' => $this->config->get('payment_payzos_sort_order'),
            ];
        }

        return $method_data;
    }
}
?>
