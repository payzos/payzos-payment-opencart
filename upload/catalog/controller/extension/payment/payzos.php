<?php

/**
 * Class ControllerExtensionPaymentPayzos
 */
class ControllerExtensionPaymentPayzos extends Controller
{
    /**
     * @var undefined
     */
    protected $api;

    /**
     * @return void
     */
    public function __construct($registry)
    {
        parent::__construct($registry);
        require_once DIR_SYSTEM . 'library/payzos/PayzosApi.php';
        $this->api = new PayzosApi();
    }

    private function error_api_output($_message)
    {
        header('Content-type: application/json');
        header('Access-Control-Allow-Origin: *');

        $output = [];
        $output['ok'] = false;
        $output['message'] = $_message;
        echo json_encode($output);
        return true;
    }

    private function data_api_output($_data)
    {
        header('Content-type: application/json');
        header('Access-Control-Allow-Origin: *');

        $output = [];
        $output['ok'] = true;
        $output['data'] = $_data;
        echo json_encode($output);
        return true;
    }

    /**
     * @return void
     */
    public function index()
    {
        $this->load->language('extension/payment/payzos');

        $data1['text_connect'] = $this->language->get('text_connect');
        $data1['text_loading'] = $this->language->get('text_loading');
        $data1['text_wait'] = $this->language->get('text_wait');

        $data1['button_confirm'] = $this->language->get('button_confirm');

        return $this->load->view('extension/payment/payzos', $data1);
    }

    public function back_url()
    {
        $data = file_get_contents('php://input');
        $data = json_decode($data, true);
        if (!isset($data['payment_id']) || !isset($_GET['order_id'])) {
            return $this->error_api_output('fill values');
        }
        $payment_id = $data['payment_id'];
        $order_id = $_GET['order_id'];
        $api_response = $this->api->get_payment($payment_id);
        if (!$api_response) {
            return $this->error_api_output('wrong payment');
        }
        if (!isset($api_response['status']) || $api_response['status'] !== 'approved') {
            return $this->error_api_output('wrong payment');
        }
        header('Content-type: application/json');
        header('Access-Control-Allow-Origin: *');
        echo json_encode([
            'redirect_url' => str_replace(
                '&amp;',
                '&',
                $this->url->link('extension/payment/payzos/payment', [
                    'order_id' => $order_id,
                    'payment_id' => $payment_id,
                ])
            ),
        ]);
        return null;
    }

    /**
     * initialize payment
     * @return void
     */
    public function confirm()
    {
        $this->load->language('extension/payment/payzos');

        $this->load->model('checkout/order');

        $wallet_hash = $this->config->get('payment_payzos_wallet_hash');
        if (!isset($this->session->data['order_id']) || !isset($wallet_hash)) {
            return $this->error_api_output('fill values');
        }

        $this->load->model('extension/payment/payzos');

        $this->load->model('checkout/order');
        $order_id = $this->session->data['order_id'];
        $order = $this->model_checkout_order->getOrder($order_id);
        if (!isset($order['order_status_id'])) {
            return $this->error_api_output('undefined order');
        }
        if ($order['order_status_id'] != 0 && $order['order_status_id'] != 1) {
            return $this->error_api_output('what do you want to do with complete order ?');
        }

        $order_amount = $this->correctAmount($order);
        $order_currency = $order['currency_code'];
        $back_url = str_replace(
            '&amp;',
            '&',
            $this->url->link('extension/payment/payzos/back_url', [
                'order_id' => $order_id,
            ])
        );
        $api_response = $this->api->make_payment(
            $wallet_hash,
            $order_amount,
            $order_currency,
            $back_url
        );
        if (
            !$api_response ||
            !is_array($api_response) ||
            !isset($api_response['payment_id']) ||
            !isset($api_response['url'])
        ) {
            return $this->error_api_output('payzos error');
        }
        return $this->data_api_output($api_response);
    }

    private function payment_view($_data, $_key, $_value, $_return = false)
    {
        if (!isset($_data['error'])) {
            $_data[$_key] = $_value;
        }
        if ($_return) {
            $this->document->setTitle($this->language->get('text_title'));

            $_data['column_left'] = $this->load->controller('common/column_left');
            $_data['column_right'] = $this->load->controller('common/column_right');
            $_data['content_top'] = $this->load->controller('common/content_top');
            $_data['content_bottom'] = $this->load->controller('common/content_bottom');
            $_data['footer'] = $this->load->controller('common/footer');
            $_data['header'] = $this->load->controller('common/header');

            $this->response->setOutput(
                $this->load->view('extension/payment/payzos_confirm', $_data)
            );
        }
        return $_data;
    }

    public function payment()
    {
        $data = [];
        $this->load->language('extension/payment/payzos');
        $data = $this->payment_view($data, 'heading_title', $this->language->get('text_title'));
        $data = $this->payment_view($data, 'breadcrumbs', []);
        $data = $this->payment_view($data, 0, [
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home', '', true),
        ]);
        if ($this->session->data['payment_method']['code'] !== 'payzos') {
            return $this->payment_view(
                $data,
                'error',
                $this->language->get('error_not_found'),
                true
            );
        }
        if (
            !isset($_GET['order_id']) ||
            !isset($this->session->data['order_id']) ||
            intval($_GET['order_id']) != $this->session->data['order_id']
        ) {
            return $this->payment_view(
                $data,
                'error',
                $this->language->get('error_order_id'),
                true
            );
        }
        $order_id = $this->session->data['order_id'];
        if (!isset($_GET['payment_id'])) {
            return $this->payment_view(
                $data,
                'error',
                $this->language->get('error_payment_id'),
                true
            );
        }
        $payment_id = $_GET['payment_id'];
        $payment = $this->api->get_payment($payment_id);
        if (!$payment) {
            return $this->payment_view(
                $data,
                'error',
                $this->language->get('error_payment_unvalid'),
                true
            );
        }
        if (
            !isset($payment["payment_id"]) ||
            !isset($payment["transaction_hash"]) ||
            !isset($payment["update_time"]) ||
            !isset($payment["real_amount"]) ||
            !isset($payment["status"]) ||
            !isset($payment["amount"])
        ) {
            return $this->payment_view(
                $data,
                'error',
                $this->language->get('Payment is unvalid'),
                true
            );
        }
        if ($payment['status'] !== 'approved') {
            return $this->payment_view(
                $data,
                'error',
                $this->language->get('Payment is not completed'),
                true
            );
        }
        $this->load->model('checkout/order');
        $order = $this->model_checkout_order->getOrder($order_id);
        if (!$order) {
            return $this->payment_view(
                $data,
                'error',
                $this->language->get('error_order_unvalid'),
                true
            );
        }
        $order_amount = $this->correctAmount($order);
        $order_currency = $order['currency_code'];
        $real_amount = $order_amount . ' ' . $order_currency;
        if ($payment['status'] !== 'approved') {
            return $this->payment_view(
                $data,
                'error',
                $this->language->get('error_payment_not_completed'),
                true
            );
        }
        if ($real_amount != $payment['real_amount']) {
            return $this->payment_view(
                $data,
                'error',
                $this->language->get('error_order_unvalid'),
                true
            );
        }
        $comment = "Payment id : {$payment_id}";
        if ($order['order_status_id'] == 0 || $order['order_status_id'] == 1) {
            try {
                $this->model_checkout_order->addOrderHistory(
                    $order_id,
                    $this->config->get('payment_payzos_order_status_id'),
                    $comment,
                    true
                );
            } catch (\Throwable $th) {
                return $this->payment_view(
                    $data,
                    'error',
                    $this->language->get('error_call_admin'),
                    true
                );
            }
        }

        $data = $this->payment_view($data, 'payment_id', $payment_id);
        $data = $this->payment_view($data, 'transaction_hash', $payment['transaction_hash']);
        $data = $this->payment_view($data, 'xtz_amount', $payment['amount'] / 1000000);
        $data = $this->payment_view($data, 'real_amount', $payment['real_amount']);
        $data = $this->payment_view(
            $data,
            'date',
            date('"M-D-Y h:m:s A', $payment['update_time'] - date('Z')) . 'UTC'
        );
        $data = $this->payment_view(
            $data,
            'button_continue',
            $this->language->get('button_complete')
        );
        $data = $this->payment_view($data, 'continue', $this->url->link('checkout/success'));
        return $this->payment_view($data, 'results', 'HI', true);
    }

    private function correctAmount($order_info)
    {
        $amount = $this->currency->format(
            $order_info['total'],
            $order_info['currency_code'],
            $order_info['currency_value'],
            false
        );
        // $amount = round($amount);
        $amount = $this->currency->convert($amount, $order_info['currency_code'], 'TOM');
        return (float) $amount;
    }
}
